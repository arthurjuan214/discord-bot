const { Client, Intents } = require('discord.js')
const config = require('../config.json')
const CommandHandler = require('./commandHandler')

const client = new Client({ intents: [Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES] })
client.login(config.Token)
    .then(r => console.log('[+] Bot started'))
    .catch(err => console.log(`[-] Error: ${err}`))


const prefix = "#"
//COMMANDS
client.on("messageCreate", (message) => {
    if (message.author.bot) return;
    if ( ! message.content.startsWith(prefix)) return;

    const commandBody = message.content.slice(prefix.length);
    const args = commandBody.split(' ');
    const command = args.shift().toLowerCase();

    CommandHandler.Handler(command, message)

});

